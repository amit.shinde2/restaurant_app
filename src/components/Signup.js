import React, { useContext, useRef } from "react";
import classes from "./Signup.module.css";
import { Link, useNavigate } from "react-router-dom";
import AuthContext from "../context/auth-context";
import LoginSignup from "../services/login-signup";

const Signup = () => {
  const nameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();
  const navigate = useNavigate();
  const authCtx = useContext(AuthContext);
  
  const SignupSubmitHandler = (event) => {
    event.preventDefault();
    
    if(nameRef.current.value && emailRef.current.value &&  passwordRef.current.value){
      const newLoggedInUserUser = {
        name: nameRef.current.value,
        email: emailRef.current.value,
        isAdmin: false,
        password: passwordRef.current.value,
        role: "user",
      };
      
      //Api POST 
      LoginSignup.postSignupUser(newLoggedInUserUser).then((res) => {
        console.log(res);
      }).catch((err) => {
        console.log(err);
      }).finally(() => {
        console.log("User signed in(details post)") 
      });


    authCtx.setRegularUsersList((prevState) => [
      ...prevState,
      newLoggedInUserUser,
    ]);

    navigate("/login");
  }else{
    alert('All fields are requierd!!');
  }
  };

  return (
    <form onSubmit={SignupSubmitHandler} className={classes.signup}>
      <p>Signup Here</p>
      <label htmlFor="name">Name</label>
      <input ref={nameRef} id="name"></input>

      <label htmlFor="email">Email</label>
      <input type="email" ref={emailRef} id="email"></input>

      <label htmlFor="password">Password</label>
      <input type="password" ref={passwordRef} id="password"></input>

      <button type="submit">Submit</button>

      <Link to="/login">Login ?</Link>
    </form>
  );
};

export default Signup;
