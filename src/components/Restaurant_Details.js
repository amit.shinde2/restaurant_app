import React, { useState } from "react";
import { useParams } from "react-router-dom";
import classes from "./Restaurant_Details.module.css";
import image from "../images/restaurant.jpg";
import ReadOnlyRating from "./Ratings/Read_Only_Rating";
import Box from "@mui/material/Box";
import Rating from "@mui/material/Rating";
import Button from "@material-ui/core/Button";
import RestaurantList from "../services/restaurants-list";
import DeleteIcon from "@mui/icons-material/Delete";

const Restaurant_Details = ({  setRestaurantsList }) => {
  const params = useParams();

  const [editRestaurantModeIsOn, setEditRestaurantModeIsOn] = useState(false);

  let arr = JSON.parse(localStorage.getItem("restaurants") || "[]");
  let clickedRestaurant = arr.filter((restaurant) => {
    return restaurant.id === params.id;
  });

  console.log(clickedRestaurant[0]);

  const EditRestaurant = (event) => {
    setEditRestaurantModeIsOn(!editRestaurantModeIsOn);
    if (editRestaurantModeIsOn) {
      RestaurantList.updateRestaurant(clickedRestaurant[0].id)
        .then((res) => {
          console.log(res);
          console.log("Updated Successfully");
        })
        .finally(() => {});

      RestaurantList.fetchRestaurantsById(clickedRestaurant[0].id)
        .then((res) => {
          console.log(res);
          console.log("User fetched by id successfully");
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {});
    }
  };
  const commentSubmitHandler = (event) => {
    event.preventDefault();
  };

  //Title and description change handler
  const titleChangeHandler = (event) => {
    console.log(event.target);
    setRestaurantsList((prevState) => {
      const arr = prevState.map((restaurant) => {
        if (restaurant.id === event.target.id) {
          if (event.target.name === "title") {
            return { ...restaurant, title: event.target.value };
          } else if (event.target.name === "description") {
            return { ...restaurant, description: event.target.value };
          } else {
            return { ...restaurant };
          }
        } else {
          return { ...restaurant };
        }
      });
      localStorage.setItem("restaurants", JSON.stringify(arr));
      return arr;
    });
  };

  //Ratings change handler
  const highestRatingChangeHandler = (event) => {
    setRestaurantsList((prevState) => {
      const arr = prevState.map((restaurant) => {
        if (restaurant.id === event.target.name) {
          return {
            ...restaurant,
            highestRatingDetails: {
              ...restaurant.highestRatingDetails,
              highestRating: event.target.value,
            },
          };
        } else {
          return { ...restaurant };
        }
      });
      localStorage.setItem("restaurants", JSON.stringify(arr));
      return arr;
    });
  };

  const lowestRatingChangeHandler = (event) => {
    setRestaurantsList((prevState) => {
      const arr = prevState.map((restaurant) => {
        if (restaurant.id === event.target.name) {
          return {
            ...restaurant,
            lowestRatingDetails: {
              ...restaurant.lowestRatingDetails,
              lowestRating: event.target.value,
            },
          };
        } else {
          return { ...restaurant };
        }
      });
      localStorage.setItem("restaurants", JSON.stringify(arr));
      return arr;
    });
  };

  const latestRatingChangeHandler = (event) => {
    console.log(event.target);
    setRestaurantsList((prevState) => {
      const arr = prevState.map((restaurant) => {
        if (restaurant.id === event.target.name) {
          return {
            ...restaurant,
            latestRatingDetails: {
              ...restaurant.latestRatingDetails,
              latestRating: event.target.value,
            },
          };
        } else {
          return { ...restaurant };
        }
      });
      localStorage.setItem("restaurants", JSON.stringify(arr));
      return arr;
    });
  };

  //Rating delete handler
  const deleteLowestRatingHandler = () => {
    console.log("lowest");
  };

  return !editRestaurantModeIsOn ? (
    <div className={classes.detailsPage}>
      <div className={classes.card} key={clickedRestaurant[0].id}>
        <img src={image} alt="restaurant" />
        <p style={{ marginTop: "20px" }}>Name: {clickedRestaurant[0].title}</p>
        <p className={classes.desc}>
          Desciption: {clickedRestaurant[0].description}
        </p>
        <p className={classes.loc}>Location: {clickedRestaurant[0].location}</p>

        <p style={{ marginTop: "20px" }}>
          Average Rating:{" "}
          <ReadOnlyRating
            value={
              (Number(clickedRestaurant[0].highestRatingDetails.highestRating) +
                Number(clickedRestaurant[0].lowestRatingDetails.lowestRating)) /
              2
            }
          />
        </p>

        <button
          style={{ borderStyle: "none", backgroundColor: "cadetblue" }}
          onClick={EditRestaurant}
        >
          Edit Details?
        </button>
      </div>
      <form
        style={{ display: "inline-block", margin: "0 35%" }}
        onSubmit={commentSubmitHandler}
      >
        <input style={{ width: "500px", height: "100px" }}></input>
        <Button variant="contained" color="primary" type="submit">
          Comment
        </Button>
      </form>

      {/* <ul style={{ width: "50%", justifyContent: "center" }}> */}
      <div className={classes.ratings}>
        <div className={classes.lowRating}>
          <p>
            Lowest Rating:{" "}
            <ReadOnlyRating
              value={clickedRestaurant[0].lowestRatingDetails.lowestRating}
            />{" "}
          </p>

          <h3>
            {clickedRestaurant[0].lowestRatingDetails.reviewerName}{" "}
            <span>{clickedRestaurant[0].lowestRatingDetails.date}</span>{" "}
          </h3>
          <p>{clickedRestaurant[0].lowestRatingDetails.review}</p>
        </div>

        <div className={classes.lowRating}>
          <p>
            Hightes Rating:{" "}
            <ReadOnlyRating
              value={clickedRestaurant[0].highestRatingDetails.highestRating}
            />
          </p>

          <h3>
            {clickedRestaurant[0].highestRatingDetails.reviewerName}{" "}
            <span>{clickedRestaurant[0].highestRatingDetails.date}</span>
          </h3>
          <p>{clickedRestaurant[0].highestRatingDetails.review}</p>
        </div>

        <div className={classes.lowRating}>
          <p>
            Latest Rating:{" "}
            <ReadOnlyRating
              value={clickedRestaurant[0].latestRatingDetails.latestRating}
            />
          </p>

          <h3>
            {clickedRestaurant[0].latestRatingDetails.reviewerName}{" "}
            <span>{clickedRestaurant[0].latestRatingDetails.date}</span>
          </h3>
          <p>{clickedRestaurant[0].latestRatingDetails.review}</p>
        </div>
      </div>
    </div>
  ) : (
    <div className={classes.detailsPage}>
      <div className={classes.card} key={clickedRestaurant[0].id}>
        <img src={image} alt="restaurant" />
        <input
          id={clickedRestaurant[0].id}
          style={{ marginToinput: "20px" }}
          onChange={titleChangeHandler}
          value={clickedRestaurant[0].title}
          name="title"
        ></input>
        <input
          className={classes.desc}
          id={clickedRestaurant[0].id}
          onChange={titleChangeHandler}
          value={clickedRestaurant[0].description}
          name="description"
        ></input>
        <p className={classes.loc}>Location: {clickedRestaurant[0].location}</p>

        <p style={{ marginTop: "20px" }}>
          Average Rating:{" "}
          <ReadOnlyRating
            value={
              (Number(clickedRestaurant[0].highestRatingDetails.highestRating) +
                Number(clickedRestaurant[0].lowestRatingDetails.lowestRating)) /
              2
            }
          />
        </p>

        <button
          style={{ borderStyle: "none", backgroundColor: "cadetblue" }}
          onClick={EditRestaurant}
        >
          Save Details?
        </button>
      </div>
      <form
        style={{ display: "inline-block", margin: "0 35%" }}
        onSubmit={commentSubmitHandler}
      >
        <input
          className={classes.comment}
          style={{ width: "500px", height: "100px" }}
        ></input>
        <Button variant="contained" color="primary" type="submit">
          Comment
        </Button>
      </form>
      <div className={classes.ratings}>
        <div className={classes.lowRating}>
          <Box>
            <p>
              Lowest Rating:
              <DeleteIcon
                onClick={deleteLowestRatingHandler}
                className={classes.deleteIcon}
              />
              <Rating
                name={clickedRestaurant[0].id}
                value={clickedRestaurant[0].lowestRatingDetails.lowestRating}
                onChange={lowestRatingChangeHandler}
                precision={0.5}
              />
            </p>

            <h3>
              {clickedRestaurant[0].lowestRatingDetails.reviewerName}{" "}
              <span>{clickedRestaurant[0].lowestRatingDetails.date}</span>
            </h3>
            <p>{clickedRestaurant[0].lowestRatingDetails.review}</p>
          </Box>
        </div>

        <div className={classes.lowRating}>
          <Box>
            <p>
              Highest Rating:
              <DeleteIcon className={classes.deleteIcon} />
              <Rating
                name={clickedRestaurant[0].id}
                value={clickedRestaurant[0].highestRatingDetails.highestRating}
                onChange={highestRatingChangeHandler}
                precision={0.5}
              />
            </p>

            <h3>
              {clickedRestaurant[0].highestRatingDetails.reviewerName}
              <span>{clickedRestaurant[0].highestRatingDetails.date}</span>
            </h3>
            <p>{clickedRestaurant[0].highestRatingDetails.review}</p>
          </Box>
        </div>

        <div className={classes.lowRating}>
          <Box>
            <p>
              Latest Rating:
              <DeleteIcon className={classes.deleteIcon} />
              <Rating
                name={clickedRestaurant[0].id}
                value={clickedRestaurant[0].latestRatingDetails.latestRating}
                onChange={latestRatingChangeHandler}
                precision={0.5}
              />
            </p>

            <h3>
              {clickedRestaurant[0].latestRatingDetails.reviewerName}{" "}
              <span>{clickedRestaurant[0].latestRatingDetails.date}</span>
            </h3>
            <p>{clickedRestaurant[0].latestRatingDetails.review}</p>
          </Box>
        </div>
      </div>
    </div>
  );
};

export default Restaurant_Details;
