import React, { useContext, useEffect, useRef, useState } from "react";
import classes from "./Users.module.css";
import { Link, useNavigate } from "react-router-dom";
import AuthContext from "../context/auth-context";
import Button from "@material-ui/core/Button";
import UsersListServices from "../services/users-list";

const Users = ({}) => {
  const nameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();
  const navigate = useNavigate();
  const authCtx = useContext(AuthContext);

  const logoutHandler = () => {
    localStorage.removeItem("users");
    authCtx.setUser({ email: "", isAdmin: false, role: "" });
    navigate("/login");
  };

  const [Users, setUsers] = useState([]);

  useEffect(() => {
    //Fetching users(if any ) fro the backend
    UsersListServices.getUsers()
      .then((res) => {
        console.log(res);
        setUsers(res);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        console.log("Users fetched ssuccessfully");
      });
  }, []);

  const addUserHandler = (event) => {
    event.preventDefault();
    if (
      nameRef.current.value &&
      emailRef.current.value &&
      passwordRef.current.value
    ) {
      const newUser = {
        name: nameRef.current.value,
        email: emailRef.current.value,
        isAdmin: false,
        password: passwordRef.current.value,
        role: "user",
      };

      //Adding user by sending it's deatils to the backend
      UsersListServices.addUsers(newUser)
        .then((res) => {
          console.log(res);
          console.log("User added successfully");
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {});

      UsersListServices.getUsers()
        .then((res) => {
          console.log(res);
          console.log("Users fetched ssuccessfully");
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {});

      authCtx.setRegularUsersList((prevState) => [...prevState, newUser]);
      nameRef.current.value = "";
      emailRef.current.value = "";
      passwordRef.current.value = "";
    } else {
      alert("All fields are required");
    }
  };

  const userDeleteHandler = (id) => {
    const deletedUser = Users.filter((user) => {
      return user.id === id;
    });

    UsersListServices.deleteUsers(deletedUser)
      .then((res) => {
        console.log(res);
        console.log("Delete user successsfullyy");
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {});

    UsersListServices.getUsers()
      .then((res) => {
        console.log(res);
        console.log("Users fetched ssuccessfully");
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {});
  };

  const updateUserHandler = (id) => {
    const updateUser = Users.filter((user) => {
      return user.id === id;
    });

    UsersListServices.updateUsers(updateUser)
      .then((res) => {
        console.log(res);
        console.log("Update user successsfullyy");
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
      });

      UsersListServices.getUsers()
      .then((res) => {
        console.log(res);
        console.log("Users fetched ssuccessfully");
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
      });
  };

  return (
    <div className={classes.users}>
      <ul className={classes.navbar}>
        <li>
          <Link to="/">Restaurants</Link>
        </li>
        <li>
          <Link to="/users">Users</Link>
        </li>
        <li>
          <a style={{ color: "#fff" }} onClick={logoutHandler}>
            Logout
          </a>
        </li>
      </ul>
      {/* Users from localstorage */}
      {/* {authCtx.regularUsersList.map((user) => {
        return (
          <div className={classes.usercard}>
            <p>Name: {user.name}</p>
            <p>Email: {user.email}</p>
          </div>
        );
      })} */}

      {Users.map((user) => {
        return (
          <div className={classes.usercard}>
            <p className={classes.name}>Name: {user.name}</p>
            <p className={classes.email}>Email: {user.email}</p>
            {authCtx.user.isAdmin && (
              <React.Fragment>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => updateUserHandler(user.id)}
                >
                  Edit
                </Button>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => userDeleteHandler(user.id)}
                >
                  Delete
                </Button>
              </React.Fragment>
            )}
          </div>
        );
      })}

      {authCtx.user.isAdmin && (
        <form onSubmit={addUserHandler} className={classes.usersForm}>
          <label htmlFor="name">Name:</label>
          <input id="name" ref={nameRef}></input>
          <label htmlFor="email">Email:</label>
          <input type="email" id="email" ref={emailRef}></input>
          <label htmlFor="password">Password:</label>
          <input type="password" id="password" ref={passwordRef}></input>
          <Button variant="contained" color="primary" type="submit">
            Add Users?
          </Button>
        </form>
      )}
    </div>
  );
};

export default Users;
