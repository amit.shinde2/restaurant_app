import React, { useContext, useEffect, useRef, useState } from "react";
import classes from "./Login.module.css";
import AuthContext from "../context/auth-context";
import { Link, useNavigate } from "react-router-dom";
import LoginSignup from "../services/login-signup";

const Login = ({ user, setUser }) => {
  const navigate = useNavigate();
  let authCtx = useContext(AuthContext);
  const emailRef = useRef();
  const passwordRef = useRef();

  console.log(authCtx);

  const loginSubmitHandler = (event) => {
      //onSubmitClick => FE validation-> true -> BE -> res-> true -> redirect to page -.  
                 //           fail-> error; 
    event.preventDefault();

    if (emailRef.current.value && passwordRef.current.value) {
      const newLoggedInUserUser = [
        {
          email: emailRef.current.value,
          isAdmin: false,
          password: passwordRef.current.value,
          role: "user",
        },
      ];
      authCtx.setUser({
        email: emailRef.current.value,
        isAdmin: false,
        password: passwordRef.current.value,
        role: "user",
      });
      localStorage.setItem("users", JSON.stringify(newLoggedInUserUser));
      navigate("/");
    } else {
      alert('All fields are required!');
    }
    
    //Api call for login-GET USER
    LoginSignup.getLoggedInUser().then((res) => {
      console.log(res)
    }).catch((err) => {
      console.log(err);
    }).finally(() => {
      console.log("User fetched")
    })

  };

  return (
    <form onSubmit={loginSubmitHandler} className={classes.login}>
      <p>Login Here</p>
      <label htmlFor="email">Email</label>
      <input type="email" id="email" ref={emailRef}></input>

      <label htmlFor="password">Password</label>
      <input type="password" id="password" ref={passwordRef}></input>

      <button type="submit">Submit</button>

      <Link to="/signup">Signup ?</Link>
    </form>
  );
};

export default Login;
