import React, { useState, useEffect, useContext, useRef } from "react";
import classes from "./Restaurant.module.css";
import image from "../images/restaurant.jpg";
import RestaurantList from "../services/restaurants-list";
import AdminAuth from "../services/admin";
import Pagination from "./Pagination";
import { Link, useNavigate } from "react-router-dom";
import AuthContext from "../context/auth-context";
import ReadOnlyRating from "./Ratings/Read_Only_Rating";
import AddRestauarant from "./Add_Restauarant";

const Restaurant = ({ restaurantsList, setRestaurantsList }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [restaurantsPerPage, setRestaurantsPerPage] = useState(5);
  const navigate = useNavigate();
  let authCtx = useContext(AuthContext);
  const [editModeIsOn, setEditModeIsOn] = useState(false);
  const [currentId, setCurrentId] = useState();

  let restaurantsLocalStorage = JSON.parse(
    localStorage.getItem("restaurants") || "[]"
  );

  const FetchRes2 = React.useCallback(() => {
    if (restaurantsLocalStorage.length <= 0) {
      RestaurantList.fetchRestaurants()
        .then((res) => {
          localStorage.setItem("restaurants", JSON.stringify(res));
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
          let arr = JSON.parse(localStorage.getItem("restaurants") || "[]");
          setRestaurantsList(arr);
          console.log("Restaurants fetched successfully");
        });
    } else {
      setRestaurantsList(restaurantsLocalStorage);
    }

    //Getting auth users
    if (authCtx.user.isAdmin !== false) {
      AdminAuth.getAuthUser().then((res) => {
        localStorage.setItem("users", JSON.stringify(res));
      });
    }
  });

  const [tick, setTick] = useState(false);

  useEffect(() => {
    // Getting Restaurants
    FetchRes2();
    RestaurantList.fetchRestaurants().then((res) => {
      console.log(res);
    });
  }, [tick]);

  //Get current restaurants
  const indexOfLastRestaurant = currentPage * restaurantsPerPage;
  const indexOfFirstRestaurant = indexOfLastRestaurant - restaurantsPerPage;
  const currentRestaurants = restaurantsList.slice(
    indexOfFirstRestaurant,
    indexOfLastRestaurant
  );

  //Restaurant Delete Handler
  const deleteHandler = (event) => {
    if (restaurantsLocalStorage) {
      const newRestaurantList = restaurantsList.filter((restaurant) => {
        return restaurant.id !== event.target.id;
      });
      setRestaurantsList(newRestaurantList);
      localStorage.setItem("restaurants", JSON.stringify(newRestaurantList));
    }

    RestaurantList.deleteRestaurant(event.target.id)
      .then((res) => {
        console.log(res);
        console.log("Deleted services");
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {});

    RestaurantList.fetchRestaurants()
      .then((res) => {
        console.log(res);
        console.log("Restaurants fetched successfully");
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {});
  };

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const EditCard = (item) => {
    navigate(`./restaurant-detail/${item.id}`);
  };

  const logoutHandler = () => {
    localStorage.removeItem("users");
    authCtx.setUser({ email: "", isAdmin: false, role: "" });
    navigate("/login");
  };

  //Edit restaurant deatils on homepage
  const EditDetailsOnTheGo = (event) => {
    setEditModeIsOn(!editModeIsOn);
    setCurrentId(event.target.id);

    const updatedRestaurantDetails = restaurantsList.filter((restaurant) => {
      return restaurant.id === currentId;
    });

    if (editModeIsOn) {
      RestaurantList.updateRestaurant(currentId, updatedRestaurantDetails)
        .then((res) => {
          console.log(res);
          console.log("Updated Successfully");

          RestaurantList.fetchRestaurants()
            .then((res) => {
              console.log(res);
              console.log("User fetched successfully");
            })
            .catch((err) => {
              console.log(err);
            })
            .finally(() => {});
        })
        .finally(() => {});
    }
  };

  //title and description change handler on homepage
  const titleChangeHandler = (event) => {
    setRestaurantsList((prevState) => {
      const arr = prevState.map((restaurant) => {
        if (restaurant.id === currentId) {
          if (event.target.name === "title") {
            return { ...restaurant, title: event.target.value };
          } else if (event.target.name === "description") {
            return { ...restaurant, description: event.target.value };
          } else if (event.target.name === "location") {
            return { ...restaurant, location: event.target.value };
          }
        } else {
          return { ...restaurant };
        }
      });
      localStorage.setItem("restaurants", JSON.stringify(arr));
      return arr;
    });
  };

  return (
    <div>
      <ul className={classes.navbar}>
        <li>
          <Link to="/">Restaurants</Link>
        </li>
        {authCtx.user.isAdmin && (
          <li>
            <Link to="/users">Users</Link>
          </li>
        )}
        <li>
          <a style={{ color: "#fff" }} onClick={logoutHandler}>
            Logout
          </a>
        </li>
      </ul>
      {currentRestaurants.map((item) => {
        return (
          <div className={classes.card} key={item.id}>
            <img src={image} alt="restaurant" />

            {currentId !== item.id ? (
              <React.Fragment>
                <p style={{ marginTop: "20px" }}>Name: {item.title}</p>
                <p className={classes.desc}>Description: {item.description}</p>
                <p className={classes.loc}>Location: {item.location}</p>
              </React.Fragment>
            ) : editModeIsOn ? (
              <React.Fragment>
                <input
                  style={{ marginTop: "20px" }}
                  name="title"
                  value={item.title}
                  onChange={titleChangeHandler}
                ></input>
                <input
                  className={classes.desc}
                  value={item.description}
                  name="description"
                  onChange={titleChangeHandler}
                ></input>
                <input
                  name="location"
                  className={classes.loc}
                  value={item.location}
                  onChange={titleChangeHandler}
                ></input>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <p style={{ marginTop: "20px" }}>Name: {item.title}</p>
                <p className={classes.desc}>Description: {item.description}</p>
                <p className={classes.loc}>Location: {item.location}</p>
              </React.Fragment>
            )}

            <p style={{ marginTop: "20px" }}>
              Average Rating:{" "}
              <ReadOnlyRating
                value={
                  (Number(item.highestRatingDetails.highestRating) +
                    Number(item.lowestRatingDetails.lowestRating)) /
                  2
                }
              />
            </p>
            {authCtx.user.isAdmin && (
              <React.Fragment>
                <button
                  className={classes.edit}
                  onClick={EditDetailsOnTheGo}
                  id={item.id}
                >
                  {editModeIsOn && currentId === item.id ? "Save" : "Edit"}
                </button>
                <button
                  className={classes.delete}
                  id={item.id}
                  onClick={deleteHandler}
                >
                  Delete
                </button>
                <button
                  className={classes.showDetails}
                  id={item.id}
                  onClick={() => EditCard(item)}
                >
                  More details?
                </button>
              </React.Fragment>
            )}
          </div>
        );
      })}
      {/* //Add restaurant */}
      {authCtx.user.isAdmin && (
        <AddRestauarant
          setRestaurantsList={setRestaurantsList}
          restaurantsList={restaurantsList}
          reload={() => setTick((tick) => !tick)} //for fetching restaurant
        />
      )}{" "}
      {/* //Pagination       */}
      <Pagination
        postsPerPage={restaurantsPerPage}
        paginate={paginate}
        totalPosts={restaurantsList.length}
      />
    </div>
  );
};

export default Restaurant;
