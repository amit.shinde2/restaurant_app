import * as React from "react";
import Box from "@mui/material/Box";
import Rating from "@mui/material/Rating";
import Typography from "@mui/material/Typography";

export default function ReadOnlyRating({ value }) {
  //   const [value, setValue] = React.useState(3.5);

  return (
    <Box>
      <Rating precision={0.5} name="read-only" value={value} readOnly />
    </Box>
  );
}
