import * as React from 'react';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

export default function ControlledRating({ value, onChange, id }) {
  const [valuee, setValue] = React.useState(value);

  console.log(valuee)

  return (
    <Box
      sx={{
        '& > legend': { mt: 2 },
      }}
    >
      <Rating
        name="simple-controlled"
        value={valuee}
        onChange={(event, newValue) => {
          console.log(newValue);
          setValue(newValue);
        }}
        precision={0.5}
      />
  
    </Box>
  );
}