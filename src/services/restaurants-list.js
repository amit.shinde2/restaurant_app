import * as Faker from "faker";
import { v4 as uuidv4 } from "uuid";

export default class RestaurantList {

  //Not Used Anymore
  // static fetchRestaurants = async () => {
  //   //todo add page
  //   // if (minPrice <= 0 || maxPrice >= 1000 || minPrice > maxPrice) throw new Error("No Books found");
  //   const restaurants = [];
  //   for (let i = 0; i < 30; i++) {
  //     let hr = Math.floor(Math.random() * 3) + 3;
  //     let lr = Math.floor(Math.random() * 3) + 1;
  //     const restaurant = {
  //       //todo add review details like review, userid, username, date. for all 3
  //       //handle case when restraunt is newly created ie. it has no review.. UI ......
  //       id: uuidv4(),
  //       title: Faker.company.companyName(),
  //       description: Faker.company.catchPhrase(),
  //       location: Faker.address.country(),
  //       highestRating: hr,
  //       lowestRating: lr,
  //       latestRating: Math.floor(Math.random() * (hr - lr)) + lr,
  //     };
  //     restaurants.push(restaurant);
  //   }

  //   return restaurants;
  // };

  static fetchRestaurants = async () => {
    //todo add page
    function randomDate(start, end) {
      return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }

    const restaurants = [];
    for (let i = 0; i < 30; i++) {
      let hr = Math.floor(Math.random() * 3) + 3;
      let lr = Math.floor(Math.random() * 3) + 1;
      const restaurant = {
        //todo add review details like review, userid, username, date. for all 3
        //handle case when restraunt is newly created ie. it has no review.. UI ......
        id: uuidv4(),
        title: Faker.company.companyName(),
        description: Faker.company.catchPhrase(),
        location: Faker.address.country(),
        highestRatingDetails: {
          highestRating: hr,
          reviewerName: "Rohit Kumar",
          review:" Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco labor nisi ut aliquip ex ea commodo consequat.",
          date: randomDate(new Date(2022, 0, 1), new Date()),
        },
        lowestRatingDetails: {
          lowestRating: lr,
          reviewerName: "Sumit Singh",
          review:
            " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco labor nisi ut aliquip ex ea commodo consequat.",
          date: randomDate(new Date(2022, 0, 1), new Date()),
        },
        latestRatingDetails: {
          latestRating: Math.floor(Math.random() * (hr - lr)) + lr,
          reviewerName: "Sonali Gupta",
          review:
            " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco labor nisi ut aliquip ex ea commodo consequat.",
          date: randomDate(new Date(2022, 0, 1), new Date()),
        },
      };
      restaurants.push(restaurant);
    }

    return restaurants;
  };

  static deleteRestaurant = async (id) => {
    //todo: use this format for BE response.. show Toast on FE accordingly, Apply Reload logic acc.

    return {
      data: {
        message: "Delete Successful",
      },
    };

    // return {
    //   data: {
    //     error: true,
    //     message: "Delete Failed",
    //   },
    // };
    // return id;
  };

  static updateRestaurant = async (id, updatedDetails) => {
    //TOOD:
    return {
      data: {
        message: "Update Restaurant Successful",
      },
      updateRestaurantDetails: updatedDetails
    };
    // return {id, updatedDetails};
  };

  static addNewRestaurant = async (newRestaurant) => {
    return {
      data: {
        message: "Add new restaurant Successful",
      },
      restaurantDetails: newRestaurant
    };
  };

  static fetchRestaurantsById = async (restaurantId) => {
    return restaurantId;
  };
}
