export default class UsersListServices {
  static getUsers = async () => {
    const users = [];
    //remove password from response
    // admin can reset password.....

    users.push({
      id: 0,
      name: "Amit",
      email: "amit@gmail.com",
      role: "user",
      isAdmin: false,
    });
    users.push({
      id: 1,
      name: "Rohit",
      email: "rohit@gmail.com",
      role: "user",
      isAdmin: false,
    });

    return users;
  };

  static deleteUsers = async (user) => {
    // users->user
    return {
      data: {
        message: "Delete User Successful",
      },
      deleteUserDetails: user,
    };
  };

  static updateUsers = async (updatedUser) => {
    //users->user add deatils to param
    return {
      data: {
        message: "Update User Successful",
      },
      updateUserDetails: updatedUser,
    };
  };

  static addUsers = async (newUser) => {
    return {
      data: {
        message: "Add User Successful",
      },
      newUserDetails: newUser,
    };
  };
}
