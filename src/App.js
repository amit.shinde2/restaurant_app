import "./App.css";
import React, { useContext, useEffect, useState } from "react";
import Restaurant from "./components/Restaurant";
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Restaurant_Details from "./components/Restaurant_Details";
import AuthContext from "./context/auth-context";
import Users from "./components/Users";

function App() {
  const [user, setUser] = useState([]);
  let userInLocalStorage = JSON.parse(localStorage.getItem("users")); //for admin localStorage
  const [restaurantsList, setRestaurantsList] = useState([]);
  const [regularUsersList, setRegularUsersList] = useState([]);

  useEffect(() => {
    const newUser = JSON.parse(localStorage.getItem("users"));
    setUser(newUser);
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        regularUsersList,
        setRegularUsersList,
      }}
    >
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            exact
            element={
              <Restaurant
                restaurantsList={restaurantsList}
                setRestaurantsList={setRestaurantsList}
              />
            }
          />
          <Route
            path="/login"
            element={
              !userInLocalStorage ? (
                <Login user={user} setUser={setUser} />
              ) : (
                <div> A user already logged in</div>
              )
            }
          />
          <Route
            path="/signup"
            element={
              userInLocalStorage ? (
                <div>{userInLocalStorage.email} A user already logged in</div>
              ) : (
                <Signup />
              )
            }
          />
          <Route
            path="/restaurant-detail/:id"
            element={
              <Restaurant_Details
                restaurantsList={restaurantsList}
                setRestaurantsList={setRestaurantsList}
              />
            }
          />
          <Route
            path="/users"
            element={
              user.isAdmin ? (
                <Users user={user} setUser={setUser} />
              ) : (
                <div>Forbidden access</div>
              )
            }
          />
        </Routes>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default App;
